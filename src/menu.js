export default [
    {
        name: "Damage", to: "/damage", src: "damage.svg",
        submenu: [
            { name: "Damage Value", to: "#damage" },
            { name: "Stun Gauge", to: "#stun" }
        ]
    },
    {
        name: "Order", to: "/order", src: "order.svg"
    },
    {
        name: "Probability", to: "/probability", src: "probability.svg",
        submenu: [
            { name: "Critical Rate", to: "#critical" },
            { name: "Hate & AI", to: "#hate" },
            { name: "Gacha", to: "#gacha" }
        ]
    },
    {
        name: "Status", to: "/status", src: "status.svg"
    },
    {
        name: "Documents", to: "/document", src: "document.svg",
        submenu: [
            { name: "Skill 0", to: "/0" },
            { name: "Skill 1", to: "/1" },
            { name: "Skill 2", to: "/2" },
            { name: "Skill 3", to: "/3" },
            { name: "Skill 4", to: "/4" },
            { name: "Skill 5", to: "/5" },
            { name: "Skill 6", to: "/6" },
            { name: "Skill 7", to: "/7" },
            { name: "Skill 8", to: "/8" },
            { name: "Skill 9", to: "/9" },
            { name: "Skill 10", to: "/10" },
            { name: "Skill 11", to: "/11" },
            { name: "Skill 12", to: "/12" },
            { name: "Skill 13", to: "/13" },
            { name: "Skill 14", to: "/14" },
            { name: "Skill 15", to: "/15" },
            { name: "Skill 16", to: "/16" },
            { name: "Skill 17", to: "/17" },
            { name: "Skill 18", to: "/18" },
            { name: "Skill 19", to: "/19" },
            { name: "Skill 20", to: "/20" },
            { name: "Skill 21", to: "/21" },
            { name: "Skill 22", to: "/22" },
            { name: "Skill 23", to: "/23" },
            { name: "Skill 24", to: "/24" },
            { name: "Skill 25", to: "/25" },
            { name: "Skill 26", to: "/26" },
            { name: "Appendix 0", to: "/appendix0" },
            { name: "Appendix 1", to: "/appendix1" },
            { name: "Appendix 2", to: "/appendix2" },
            { name: "Appendix 3", to: "/appendix3" },
        ]
    }
]
